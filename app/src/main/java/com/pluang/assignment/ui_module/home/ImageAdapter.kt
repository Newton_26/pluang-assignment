package com.pluang.assignment.ui_module.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pluang.assignment.R
import com.pluang.assignment.data.model.PhotoInfo
import com.pluang.assignment.databinding.VhPhotoItemsBinding

class ImageAdapter(
    val onItemClick: (PhotoInfo, Int) -> Unit
) : ListAdapter<PhotoInfo, PhotoViewHolder>(diffUtils) {

    companion object {
        private val diffUtils = object : DiffUtil.ItemCallback<PhotoInfo>() {
            override fun areItemsTheSame(
                oldItem: PhotoInfo,
                newItem: PhotoInfo
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: PhotoInfo,
                newItem: PhotoInfo
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val binding = VhPhotoItemsBinding.bind(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.vh_photo_items, parent, false)
        )
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(onItemClick, item = getItem(position))
    }

}

class PhotoViewHolder(private val binding: VhPhotoItemsBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        onClick: (PhotoInfo, Int) -> Unit,
        item: PhotoInfo
    ) = itemView.apply {
        Glide.with(context)
            .load(item.imageUrl)
            .centerCrop()
            .placeholder(R.drawable.preview)
            .into(
                binding.ivPhoto
            )

        setOnClickListener {
            onClick.invoke(item, adapterPosition)
        }
    }
}
