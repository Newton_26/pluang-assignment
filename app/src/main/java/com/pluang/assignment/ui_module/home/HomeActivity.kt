package com.pluang.assignment.ui_module.home

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionInflater
import android.view.View
import android.widget.Toast
import androidx.core.app.SharedElementCallback
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pluang.assignment.R
import com.pluang.assignment.base.BaseActivity
import com.pluang.assignment.data.model.APIStatus
import com.pluang.assignment.data.model.ErrorMsg
import com.pluang.assignment.databinding.ActivityHomeBinding
import com.pluang.assignment.ui_module.photo_details.EXTRA_PARAM_ID
import com.pluang.assignment.ui_module.photo_details.PhotoDetailsActivity
import com.pluang.assignment.ui_module.photo_details.VIEW_NAME_HEADER_IMAGE
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint()
class HomeActivity : BaseActivity<ActivityHomeBinding>() {

    private var isMoreCalling = false
    private var search: String? = null
    private var currentPosition: Int = 0

    @Inject
    lateinit var viewModel : HomeViewModel

    override fun getLayoutRes(): Int {
        return R.layout.activity_home
    }

    private val imageAdapter by lazy {
        ImageAdapter { info, position ->
            this.currentPosition = position

            val intent = Intent(this, PhotoDetailsActivity::class.java)
            intent.putExtra(EXTRA_PARAM_ID, info)

            try {
                val itemView = binding.rvImages.layoutManager?.findViewByPosition(position)
                val options = ActivityOptions.makeSceneTransitionAnimation(
                    this,
                    itemView?.findViewById(R.id.ivPhoto),
                    VIEW_NAME_HEADER_IMAGE
                )

                startActivity(intent, options.toBundle())
            } catch (e: Exception) {

            }
        }
    }

    override fun onInitialise(savedInstanceState: Bundle?) {
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
        prepareTransitions()
        observeLiveData()
    }

    //Initialisation
    private fun initView() {
        binding.btnSearch.setOnClickListener {
            search = binding.inputSearch.editText?.text?.toString()

            if ((binding.rvImages.layoutManager as GridLayoutManager).spanCount > 2) {
                binding.twoColumn.performClick()
            }

            viewModel.getSearch(search)
        }

        binding.rvImages.apply {
            this.layoutManager =
                GridLayoutManager(context, 2)
            this.adapter = imageAdapter
        }

        binding.rvImages.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layout = recyclerView.layoutManager as LinearLayoutManager
                val firstVisibleItem = layout.findFirstCompletelyVisibleItemPosition()
                val visibleItemCount = layout.childCount

                val totalItem = layout.itemCount

                if (dy > 0 && !isMoreCalling) {
                    if (firstVisibleItem + visibleItemCount >= totalItem.minus(4)) {
                        viewModel.getSearchMore(search)

                        isMoreCalling = true
                    }
                }
            }
        })

        binding.twoColumn.setOnClickListener {
            val layoutManager = binding.rvImages.layoutManager as GridLayoutManager
            layoutManager.spanCount = 2

            binding.rvImages.layoutManager = layoutManager
            imageAdapter.notifyDataSetChanged()
        }

        binding.threeColumn.setOnClickListener {
            val layoutManager = binding.rvImages.layoutManager as GridLayoutManager
            layoutManager.spanCount = 3

            binding.rvImages.layoutManager = layoutManager
            imageAdapter.notifyDataSetChanged()
        }

        binding.fourColumn.setOnClickListener {
            val layoutManager = binding.rvImages.layoutManager as GridLayoutManager
            layoutManager.spanCount = 4

            binding.rvImages.layoutManager = layoutManager
            imageAdapter.notifyDataSetChanged()
        }

        binding.inputSearch.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (binding.inputSearch.isErrorEnabled) {
                    binding.inputSearch.error = null
                }
            }
        })
    }

    //getting data when liveData update values
    private fun observeLiveData() {
        var lastPosition = 0

        viewModel.searchImage.observe(this) {
            when (it.status) {
                APIStatus.LOADING -> {
                    showProgress()
                }
                APIStatus.SUCCESS -> {
                    hideProgress()
                    if (imageAdapter.itemCount == 0 && it.data?.isEmpty() == true) {
                        Toast.makeText(
                            this,
                            "No photos",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    imageAdapter.submitList(it.data)
                    imageAdapter.notifyItemChanged(lastPosition)

                    isMoreCalling = false
                    lastPosition = imageAdapter.itemCount - 1
                }
                APIStatus.ERROR -> {
                    hideProgress()
                    if (it.errorMsg is ErrorMsg) {
                        if (it.errorMsg.type == ErrorMsg.ErrType.NO_LOAD_MORE) {
                            isMoreCalling = true
                            Toast.makeText(
                                this,
                                it.errorMsg.errorMessage ?: "No more photos",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            isMoreCalling = false
                            binding.inputSearch.error = it.errorMsg.errorMessage
                        }
                    } else {
                        isMoreCalling = false
                        Toast.makeText(
                            this,
                            it.errorMsg?.cause?.toString() ?: "Error!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
    }

    /**
     * Prepares the shared element transition to the PhotoDetailsActivity, as well as the other transitions
     * that affect the flow.
     */
    private fun prepareTransitions() {
        val transitions = TransitionInflater.from(this)
            .inflateTransition(R.transition.grid_exit_transition)
        window.sharedElementExitTransition = transitions

        // A similar mapping is set at the ImagePagerFragment with a setEnterSharedElementCallback.
        setExitSharedElementCallback(
            object : SharedElementCallback() {
                override fun onMapSharedElements(
                    names: List<String>,
                    sharedElements: MutableMap<String, View>
                ) {
                    // Locate the ViewHolder for the clicked position.
                    val selectedViewHolder: RecyclerView.ViewHolder = binding.rvImages
                        .findViewHolderForAdapterPosition(currentPosition) ?: return

                    // Map the first shared element name to the child ImageView.
                    sharedElements[names[0]] =
                        selectedViewHolder.itemView.findViewById(R.id.ivPhoto)
                }
            })
    }

}