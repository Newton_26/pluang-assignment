package com.pluang.assignment.ui_module.photo_details

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.SharedElementCallback
import androidx.core.view.ViewCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.pluang.assignment.R
import com.pluang.assignment.data.model.PhotoInfo
import com.pluang.assignment.databinding.ActivityPhotoDetailsBinding

// Extra name for the ID parameter
const val EXTRA_PARAM_ID = "detail:_id"

// View name of the header image. Used for activity scene transitions
const val VIEW_NAME_HEADER_IMAGE = "detail:header:image"

class PhotoDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPhotoDetailsBinding
    private var item : PhotoInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPhotoDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
//        showTransition()
        prepareSharedElementTransition()
    }

    private fun initView() {
        item = intent.getSerializableExtra(EXTRA_PARAM_ID) as? PhotoInfo

        /*
         * Set the name of the view's which will be transition to, using the static values above.
         * This could be done in the layout XML, but exposing it via static variables allows easy
         * querying from other Activities
         */
        ViewCompat.setTransitionName(binding.ivPhoto, VIEW_NAME_HEADER_IMAGE)
        loadFullPhoto()
    }

    override fun onBackPressed() {
        finishAfterTransition()
    }

    private fun loadFullPhoto(){
        Glide.with(binding.ivPhoto.context)
            .load(item?.largeImageUrl)
            .listener(object : RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.progressBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.progressBar.visibility = View.GONE
                    return false
                }

            })
            .into(
                binding.ivPhoto
            )
    }

    /**
     * Prepares the shared element transition from and back to the grid fragment.
     */
    private fun prepareSharedElementTransition() {
        val transition = TransitionInflater.from(this)
            .inflateTransition(R.transition.image_shared_element_transition)
        window.sharedElementEnterTransition = transition

        // A similar mapping is set at the HomeActivity with a setExitSharedElementCallback.
        setEnterSharedElementCallback(
            object : SharedElementCallback() {
                override fun onMapSharedElements(
                    names: List<String>,
                    sharedElements: MutableMap<String, View>
                ) {
                    // Map the first shared element name to the child ImageView.
                    sharedElements[names[0]] = binding.ivPhoto
                }
            })
    }
}