package com.pluang.assignment.ui_module.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.pluang.assignment.base.BaseViewModel
import com.pluang.assignment.data.DataRepository
import com.pluang.assignment.data.model.APIResponse
import com.pluang.assignment.data.model.ErrorMsg
import com.pluang.assignment.data.model.PhotoInfo
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val dataRepository : DataRepository
) : BaseViewModel() {

    private val _searchImage = MutableLiveData<APIResponse<List<PhotoInfo>>>()
    val searchImage: LiveData<APIResponse<List<PhotoInfo>>> = _searchImage
    private val images = mutableListOf<PhotoInfo>()
    private var search: String? = null
    private var currentPage: Int = 1

    fun getSearch(search: String?) {
        _searchImage.postValue(APIResponse.success(mutableListOf<PhotoInfo>()))
        currentPage = 1
        images.clear()
        getSearchMore(search)
    }

    fun getSearchMore(search: String?) {
        if (search.isNullOrEmpty()) {
            val error = ErrorMsg()
            error.code = 403
            error.type = ErrorMsg.ErrType.EMPTY
            error.errorMessage = "Please add something to search"

            _searchImage.postValue(APIResponse.error(errorMsg = error))
            return
        }

        if (!this.search.equals(search)) {
            images.clear()
            this.search = null
            this.currentPage = 1
        }

        this.search = search.replace(" ","+")

        dataRepository.getSearchImages(
            searchText = search.orEmpty(),
            page = this.currentPage,
            perPage = 30
        )
            .doOnSubscribe {
                compositeDisposable.add(it)
                _searchImage.postValue(APIResponse.loading())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    println(it)
                    if ((it.pages > 0 && this.currentPage >= it.pages) || (currentPage > 1 && it.photo?.isEmpty() == true)) {
                        this.currentPage = 1

                        val error = ErrorMsg()
                        error.code = 403
                        error.type = ErrorMsg.ErrType.NO_LOAD_MORE
                        error.errorMessage = "You have touched all photos  \uD83D\uDE0A"

                        _searchImage.postValue(APIResponse.error(errorMsg = error))
                    } else {
                        this.currentPage = this.currentPage.plus(1)
                        this.images.addAll(it.photo.orEmpty())
                        _searchImage.postValue(APIResponse.success(this.images))
                    }
                },
                {
                    println(it.message)
                    _searchImage.postValue(APIResponse.error(errorMsg = it))
                }
            )
    }

}