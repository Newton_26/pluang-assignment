package com.pluang.assignment.base

import android.app.Dialog
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewbinding.ViewBinding
import com.pluang.assignment.ui_module.progress_dialog.ProgressDialog
import dagger.hilt.android.AndroidEntryPoint

abstract class BaseActivity<V : ViewBinding> : HiltActivity() {

    lateinit var binding: V
//    lateinit var viewModel: M

//    abstract fun getViewModel(): Class<M>
    @LayoutRes
    abstract fun getLayoutRes(): Int
    abstract fun onInitialise(savedInstanceState: Bundle?)

    private var progressBar: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutRes())
        setContentView(binding.root)

//        viewModel = ViewModelProvider.AndroidViewModelFactory(this.application).create(getViewModel())
        onInitialise(savedInstanceState)
        progressBar = ProgressDialog.progressDialog(this)
    }

    fun showProgress(){
        progressBar?.show()
    }
    fun hideProgress(){
        progressBar?.dismiss()
    }
}

@AndroidEntryPoint
abstract class HiltActivity : AppCompatActivity() { //Hilt has an issue for type parameters i.e. BaseActivity<M,V>

}