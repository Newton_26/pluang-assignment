package com.pluang.assignment.base

import androidx.lifecycle.ViewModel
import dagger.hilt.android.scopes.ViewModelScoped
import io.reactivex.rxjava3.disposables.CompositeDisposable

@ViewModelScoped
open class BaseViewModel : ViewModel() {

    val compositeDisposable: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}