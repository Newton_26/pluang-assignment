package com.pluang.assignment.di

import com.pluang.assignment.data.DataRepository
import com.pluang.assignment.data.DataRepositoryImpl
import com.pluang.assignment.data.web_service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent


@Module
@InstallIn(ActivityComponent::class)
object DataModule {

    @Provides
    fun provideDataRepository(apiService: ApiService): DataRepository{
        return DataRepositoryImpl(apiService)
    }
}