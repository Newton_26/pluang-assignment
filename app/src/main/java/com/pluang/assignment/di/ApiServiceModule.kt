package com.pluang.assignment.di

import com.google.gson.Gson
import com.pluang.assignment.BuildConfig
import com.pluang.assignment.data.web_service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
@InstallIn(ActivityComponent::class)
class ApiServiceModule(){

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    fun getRetrofit(
        httpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.ROOT_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(
                RxJava3CallAdapterFactory.create()
            )
            .client(httpClient)
            .build()
    }

    @Provides
    fun getGSON(): Gson {
        return Gson()
    }

    @Provides
    fun getHttpClient(): OkHttpClient {
        val okhttpBuilder = OkHttpClient.Builder()
        okhttpBuilder.readTimeout(10, TimeUnit.SECONDS)
        okhttpBuilder.connectTimeout(30, TimeUnit.SECONDS)

        return okhttpBuilder.build()
    }
}