package com.pluang.assignment.data.web_service

import com.pluang.assignment.data.model.Photos
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("api/")
    fun getImages(
//        @Query("method") method: String = "flickr.photos.search",
        @Query("key") apiKey: String = "25554624-b7a1ed66c367ddbfecb5579cf",
        @Query("image_type") format: String = "photo",
//        @Query("nojsoncallback") nojsonCallback: String = "1",
        @Query("q") searchText: String,
        @Query("per_page") perPage: Int,
        @Query("page") page: Int,
    ): Single<Photos>
}