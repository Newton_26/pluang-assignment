package com.pluang.assignment.data

import com.pluang.assignment.data.model.Photos
import io.reactivex.rxjava3.core.Single

interface DataRepository {

    fun getSearchImages(
        searchText: String,
        perPage: Int,
        page: Int
    ): Single<Photos>
}