package com.pluang.assignment.data.web_service

import com.google.gson.Gson
import com.pluang.assignment.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {

    fun getApiService(): ApiService{
        return getRetrofit().create(ApiService::class.java)
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.ROOT_URL)
            .addConverterFactory(getGSON())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(getHttpClient())
            .build()
    }

    private fun getGSON(): GsonConverterFactory {
        val gson = Gson()
        return GsonConverterFactory.create(gson)
    }

    private fun getHttpClient(): OkHttpClient{
        val okhttpBuilder = OkHttpClient.Builder()
        okhttpBuilder.readTimeout(10, TimeUnit.SECONDS)
        okhttpBuilder.connectTimeout(30, TimeUnit.SECONDS)

        return okhttpBuilder.build()
    }
}