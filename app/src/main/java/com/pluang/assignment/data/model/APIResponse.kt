package com.pluang.assignment.data.model

data class APIResponse <out T>(val status: APIStatus, val data: T?, val errorMsg: Throwable?){
    companion object{
        fun <T> loading(data: T? = null): APIResponse<T> {
            return APIResponse(APIStatus.LOADING, data, null)
        }

        fun <T> success(data: T?): APIResponse<T> {
            return APIResponse(APIStatus.SUCCESS, data, null)
        }

        fun <T> error(data: T? = null, errorMsg: Throwable? = null): APIResponse<T> {
            return APIResponse(APIStatus.ERROR,data, errorMsg)
        }
    }
}
