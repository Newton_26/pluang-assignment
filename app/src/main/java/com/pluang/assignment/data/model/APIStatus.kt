package com.pluang.assignment.data.model

enum class APIStatus {
    LOADING, SUCCESS, ERROR
}

class ErrorMsg(
    var type: ErrType? = null,
    var code: Int? = null,
    var errorMessage: String? = null
) : Throwable() {
    enum class ErrType {
        UNKNOWN,
        EMPTY,
        NO_LOAD_MORE
    }
}