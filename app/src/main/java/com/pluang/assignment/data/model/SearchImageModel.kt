package com.pluang.assignment.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SearchImageModel(
    @SerializedName("photos")
    val photos: Photos?
) : Serializable

data class Photos(
    @SerializedName("total")
    val pages: Int,
    @SerializedName("hits")
    val photo: MutableList<PhotoInfo>?
) : Serializable

data class PhotoInfo(
    @SerializedName("id")
    val id: String,
    @SerializedName("previewURL")
    val imageUrl: String,
    @SerializedName("webformatURL")
    val largeImageUrl: String
) : Serializable