package com.pluang.assignment.data

import com.pluang.assignment.data.model.Photos
import com.pluang.assignment.data.web_service.ApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton


class DataRepositoryImpl @Inject constructor(
    private val apiService : ApiService
) : DataRepository {

    override fun getSearchImages(
        searchText: String,
        perPage: Int,
        page: Int
    ): Single<Photos> {
        return apiService.getImages(
            searchText = searchText,
            perPage = perPage,
            page = page
        )
    }
}